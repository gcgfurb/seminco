// Usando o Adobe Acrobat Professional
// como usar:
//Tools
//JavaScript
//Set Document Actions...
//Document Will Save
//Edit
//Coloca o codigo no painel

////////////// CABECALHO

//var r = [100,802, 495,812];
var r = [85,792, 510,802]; //formato 2006

var numpag = 1;

//variavel i comeca em 6 pq a primeira pag. a ser numerada eh a pag. 7
for (var i = 6; i < numPages; i++) {
 
  var f1 = this.addField(String("page1-"+i+1),"text",i,r);

  f1.textSize  = 10;
  f1.textColor = color.black;
  f1.fillColor = color.transparent;
  f1.textFont = font.Times;
  f1.borderStyle = border.s;
  f1.strokeColor = color.transparent;


  if ((numpag+i) % 2) {
    // impar
    f1.alignment = "right";
    f1.value = String(numpag+i)+" "; // numpag
  }
  else {
    // par
    f1.alignment = "left";   
    f1.value = String(numpag+i);   
  }
}


////////////// RODAP�

var caixa = [85,55, 345,25]; //formato 2006
var caixa2 = [85,58, 510,57]; //linha vertical


var PGs = [7,16,
             17,28,
             29,36,
             37,50,
             51,62,
             63,72,
             73,81,
             82,91,
             92,103,
             104,116,
             117,127];

var PG;
var PGto;
var field1;
var field2;
var qtd = 12;

var i = 0;
while (i<qtd*2) {
    PG = PGs[i];
    i++;
    PGto = PGs[i];
    i++;
    field1 = this.addField(String("PG1-"+PG),"text",PG-1,caixa);
    field1.value = "Grahl, E. A; Reis, D. S. (Eds.). Anais do XVIII Semin�rio de Computa��o, Blumenau, 9-10 de dezembro, 2009. p "+PG+"-"+PGto+".";
    field1.textSize  = 10;
    field1.textColor = color.black;
    field1.fillColor = color.transparent;
    field1.textFont = font.Times;
    field1.borderStyle = border.s;
    field1.strokeColor = color.transparent;
    field1.alignment = "left";
    field1.multiline = true;
    field2 = this.addField(String("PG1-line"+PG),"text",PG-1,caixa2);
    field2.borderStyle = border.s;
    field2.lineWidth = 1
    field2.strokeColor = color.black;
}
